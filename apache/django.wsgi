import os, sys

#Calculate the path based on the location of the WSGI script.
apache_configuration= os.path.dirname(__file__)
project = os.path.dirname(apache_configuration)
workspace = os.path.dirname(project)
sys.path.append(workspace) 

# add current directory or directory with apps
sys.path.append('/home/cody/work/mysite')
sys.path.append('/home/cody/work/mysite/')

#os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.apache.settings_dev'
os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

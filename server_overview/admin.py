from django.contrib import admin
from server_overview.models import Server

admin.site.register(Server)

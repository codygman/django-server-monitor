from django.db import models

class Server(models.Model):
    # TODO: Change status to enum
    SERVER_STATUSES = (
            ('unknown', 'unknown'),
            ('online', 'online'),
            ('offline', 'offline'),
            )

    identifier = models.CharField(max_length=50, null=False, blank=False)
    ip = models.IPAddressField(null=False, blank=False)
    username = models.CharField(max_length=50, null=False, blank=False)
    password = models.CharField(max_length=50, null=False, blank=False)
    port = models.IntegerField(null=False, blank=False, default=22)
    os = models.CharField(max_length=80, null=True, blank=True)
    status = models.CharField(max_length=50, choices=SERVER_STATUSES, default='unknown')

    def __unicode__(self):
        return self.identifier


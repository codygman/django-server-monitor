from django.shortcuts import render_to_response, HttpResponseRedirect
from django.core.urlresolvers import reverse 

import thread

#from server_status_checker.objects import ServerInfo
from server_overview.models import Server
from updater.status_checker import update_server, update_all

def index(request):
    servers = Server.objects.all()
    return render_to_response('server_overview/index.html', {'servers': servers})

def force_check(request):
    #TODO: Figure out why this always shows things offline. I think it's an execution
    # limit, but not quite sure.
    try:
        thread.start_new_thread(update_all, ())
    except Exception, errtxt:
        print errtxt
    #return HttpResponseRedirect(reverse('mysite.server_overview.views.index'))
    return HttpResponseRedirect('/')

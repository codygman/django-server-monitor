from server_status_checker.objects import ServerInfo
import MySQLdb
from MySQLdb.cursors import DictCursor

def update_server(ip):
    conn = MySQLdb.connect (host = "localhost",
                            user = "root",
                            passwd = "",
                            db = "mysite")
    cursor = conn.cursor(DictCursor)
    cursor.execute ("SELECT * from server_overview_server where ip = %s;", (ip,))
    row = cursor.fetchone()
    return ServerInfo(row)



def update_all():
    conn = MySQLdb.connect (host = "localhost",
                            user = "root",
                            passwd = "",
                            db = "mysite")
    cursor = conn.cursor(DictCursor)
    cursor.execute ("SELECT ip from server_overview_server;")
    all_ips = [f["ip"] for f in cursor.fetchall()]

# generate server info objects
    server_results = []
    for ip in all_ips:
        server_results.append(update_server(ip))

    for server in server_results:
# needs refractored bad
        sql = "UPDATE server_overview_server set status = %s, os = %s where id = %s;"
        status = None
        if server.available:
            status = 'online'
        elif not server.available:
            status = 'offline'
        else:
            status = 'offline'
        cursor.execute(sql, (status, server.os, server.id))

    cursor.close ()
    conn.close ()

if __name__ == "__main__":
    update_all()

from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # main app is 'server_overview'
    (r'^$', include('server_overview.urls')),
    # admin area
    (r'^admin/', include(admin.site.urls)),
)
